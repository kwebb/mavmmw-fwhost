"""This profile instantiates the vendor AP-to-Edge setup as connected via the PhantomNet attenuator matrix.  It includes a NAT proxy node that connects to the internal (experimental net) LAN that the devices communicate on.

Instructions:
Swap in.
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.emulab as elab

PROXY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
PROXY_SCRIPT = "/local/repository/bin/proxy_start.sh"

PROXYGWADDR = "192.168.50.129"
SHVLANNAME = "MAVENIR-MMW"
SHVLANMASK = "255.255.255.0"

# Parameters
pc = portal.Context()

pc.defineParameter("proxy_hwtype", "Proxy Node Type",
                   portal.ParameterType.STRING, "d710",
                   longDescription="Hardware type for compute node that will act as the proxy. Leave blank for 'any'.")

pc.defineParameter("gwaddr", "Gateway address for proxy on shared vlan",
                   portal.ParameterType.STRING, PROXYGWADDR,
                   advanced=True)

pc.defineParameter("sharedVlanName","Shared VLAN Name",
                   portal.ParameterType.STRING,SHVLANNAME,
                   longDescription="This is the name of the shared VLAN that the separate AP experiments will attach to. YOU MUST SET THIS TO SOMETHING VALID!",
                   advanced=True)

pc.defineParameter("sharedVlanNetmask", SHVLANMASK,
                   portal.ParameterType.STRING,"255.255.255.0",
                   advanced=True)

pc.defineParameter("createSharedVlan", "Create Shared VLAN",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Create the shared VLAN. (Disable this if the VLAN already exists.)",
                   advanced=True)

pc.defineParameter("tagSharedVlan", "Use Tagged Interfaces",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Use tagged interfaces on nodes connected to the shared vlan.",
                   advanced=True)

pc.defineParameter("fixedFWNode", "Specific node to allocate",
                   portal.ParameterType.STRING, "",
                   longDescription="Specify the node_id of the node to use as the firewall.",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

# Create the "inner egress LAN"
lan1 = request.LAN("lan1")
lan1.vlan_tagging = params.tagSharedVlan
lan1.setNoBandwidthShaping()
if params.createSharedVlan:
    lan1.createSharedVlan(params.sharedVlanName)
else:
    lan1.connectSharedVlan(params.sharedVlanName)

# Request the NAT proxy node.
proxy = request.RawPC("proxy")
if params.fixedFWNode:
    proxy.component_id = params.fixedFWNode
else:
    proxy.hardware_type = params.proxy_hwtype
proxy.disk_image = PROXY_IMG
proxy.addService(pg.Execute(shell="sh", command=PROXY_SCRIPT))
pxlif = proxy.addInterface("pxlif", pg.IPv4Address(params.gwaddr, params.sharedVlanNetmask))
lan1.addInterface(pxlif)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
